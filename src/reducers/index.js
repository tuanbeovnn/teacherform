import { combineReducers } from 'redux';
import questions from './questions';

const appReducers = combineReducers({
	questions
});

export default appReducers;