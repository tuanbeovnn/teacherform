import * as types from './../contants/Actiontypes';

export const actFormSubmit = (feedback) => {
	return {
		type : types.SUBMIT,
		feedback
	}
}
