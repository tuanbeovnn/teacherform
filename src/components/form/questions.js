import React, {Component} from 'react';

class questions extends Component{ 
        render(){
             var {question} = this.props;    
            return (
            <div id="content">
                <div className="title">{question.title}</div>
                <div className="question">{question.question}</div>
            </div>
             
        );
    }
}

export default questions;